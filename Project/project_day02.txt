一、升级网站服务器192.168.4.33 和  192.168.4.44 运行平台LNMP
	1.1 清除当前配置
[root@web33 ~]# systemctl  stop  httpd
[root@web33 ~]# systemctl  disable  httpd
Removed symlink /etc/systemd/system/multi-user.target.wants/httpd.service.
[root@web33 ~]# 
[root@web33 ~]# umount /var/www/html/
[root@web33 ~]# 
[root@web33 ~]# vim /etc/fstab 
[root@web33 ~]# tail -1  /etc/fstab 
#192.168.4.30:/site	/var/www/html		nfs	defaults 0 0 
[root@web33 ~]#

	1.2 部署LNMP环境
   44  rpm -q gcc 
   45  rpm -q pcre-devel zlib-devel
   46  yum -y install gcc  pcre-devel zlib-devel
   47  tar -zxvf /var/ftp/pub/nginx-1.12.2.tar.gz 
   48  cd nginx-1.12.2/
   49  ./configure 
   50  make && make install

[root@web33 nginx-1.12.2]# ls /usr/local/nginx/
conf  html  logs  sbin
[root@web33 nginx-1.12.2]# 

[root@web33 nginx-1.12.2]# vim  +65 /usr/local/nginx/conf/nginx.conf
location ~ \.php$ {
            root           html;
            fastcgi_pass   127.0.0.1:9000;
            fastcgi_index  index.php;
            include        fastcgi.conf;
        }
:wq
[root@web33 nginx-1.12.2]# /usr/local/nginx/sbin/nginx  -t
nginx: the configuration file /usr/local/nginx/conf/nginx.conf syntax is ok
nginx: configuration file /usr/local/nginx/conf/nginx.conf test is successful
[root@web33 nginx-1.12.2]# 

[root@web33 nginx-1.12.2]# /usr/local/nginx/sbin/nginx
[root@web33 nginx-1.12.2]# 
[root@web33 nginx-1.12.2]# netstat  -utnlp  | grep 80
tcp        0      0 0.0.0.0:80              0.0.0.0:*               LISTEN      5230/nginx: master  
[root@web33 nginx-1.12.2]#

[root@web33 nginx-1.12.2]# chmod  +x /etc/rc.local 
[root@web33 nginx-1.12.2]# echo "/usr/local/nginx/sbin/nginx" >> /etc/rc.local 
[root@web33 nginx-1.12.2]# 

[root@web33 ~]# yum -y install php php-mysql php-fpm
[root@web33 ~]# systemctl  start php-fpm
[root@web33 ~]# systemctl  enable php-fpm
Created symlink from /etc/systemd/system/multi-user.target.wants/php-fpm.service to /usr/lib/systemd/system/php-fpm.service.

[root@web33 ~]# 
[root@web33 ~]# netstat  -utnlp  | grep 9000
tcp        0      0 127.0.0.1:9000          0.0.0.0:*               LISTEN      5271/php-fpm: maste 
[root@web33 ~]#

[root@web33 ~]# vim /etc/fstab
192.168.4.30:/site      /usr/local/nginx/html   nfs     defaults 0 0
:wq

[root@web33 ~]#  mount  -a 

[root@web33 ~]# mount | grep  -i  "/usr/local/nginx/html"
192.168.4.30:/site on /usr/local/nginx/html type nfs4 (rw,relatime,vers=4.1,rsize=131072,wsize=131072,namlen=255,hard,proto=tcp,port=0,timeo=600,retrans=2,sec=sys,clientaddr=192.168.4.33,local_lock=none,addr=192.168.4.30)
[root@web33 ~]#

[root@web33 ~]# vim /usr/local/nginx/html/d.php
[root@web33 ~]# cat /usr/local/nginx/html/d.php
<?php
	echo "hello world";
	echo  "\n";
?>
[root@web33 ~]# 

[root@web33 ~]# curl  http://localhost/d.php
hello world


同样的步骤 配置web44 主机 

[root@web44 nginx-1.12.2]# curl http://localhost/d.php
hello world
[root@web44 nginx-1.12.2]# 

二、 部署缓存服务
	2.1  在6台主机 运行redis服务并启用了集群配置
	2.2 创建集群
		2.2.1  配置管理主机192.168.4.58
[root@mgm58 ~]# yum -y install ruby rubygems
[root@mgm58 ~]# gem install redis-3.2.1.gem
[root@mgm58 ~]# tar -zxvf redis-4.0.8.tar.gz

[root@mgm58 ~]# cd redis-4.0.8/
[root@mgm58 redis-4.0.8]# 
[root@mgm58 redis-4.0.8]# mkdir /root/bin
[root@mgm58 redis-4.0.8]# ls src/*.rb
src/redis-trib.rb
[root@mgm58 redis-4.0.8]# cp src/redis-trib.rb /root/bin
[root@mgm58 redis-4.0.8]# chmod +x /root/bin/redis-trib.rb 
[root@mgm58 redis-4.0.8]#
[root@mgm58 redis-4.0.8]# redis-trib.rb    help  #可以查看到命令的帮助信息为成功

		2.2.2  创建集群
[root@mgm58 ~]# redis-trib.rb  create --replicas 1  192.168.4.51:6379  192.168.4.52:6379 192.168.4.53:6379 192.168.4.54:6379 192.168.4.56:6379 192.168.4.57:6379
>>> Creating cluster
>>> Performing hash slots allocation on 6 nodes...
Using 3 masters:
192.168.4.51:6379
192.168.4.52:6379
192.168.4.53:6379
Adding replica 192.168.4.56:6379 to 192.168.4.51:6379
Adding replica 192.168.4.57:6379 to 192.168.4.52:6379
Adding replica 192.168.4.54:6379 to 192.168.4.53:6379
M: 81efbb654e1f56b313c8ed873d21c680b5a20df4 192.168.4.51:6379
   slots:0-5460 (5461 slots) master
M: a3424220d0fbb87210a4aad2e9568ca0057788ab 192.168.4.52:6379
   slots:5461-10922 (5462 slots) master
M: c901339df646979b6deaeba512abfac5e7be6ec6 192.168.4.53:6379
   slots:10923-16383 (5461 slots) master
S: 279b5a30af0cb3a2277dacc8d8fd8c072a667c04 192.168.4.54:6379
   replicates c901339df646979b6deaeba512abfac5e7be6ec6
S: 441b538321343a55bcc4fb60fb0f6a9fdcb98d2b 192.168.4.56:6379
   replicates 81efbb654e1f56b313c8ed873d21c680b5a20df4
S: 07acdc56acfe9c6c41f8390e635ebab6a87d0848 192.168.4.57:6379
   replicates a3424220d0fbb87210a4aad2e9568ca0057788ab
Can I set the above configuration? (type 'yes' to accept):    yes  同意配置
>>> Nodes configuration updated
>>> Assign a different config epoch to each node
>>> Sending CLUSTER MEET messages to join the cluster
Waiting for the cluster to join...
>>> Performing Cluster Check (using node 192.168.4.51:6379)
M: 81efbb654e1f56b313c8ed873d21c680b5a20df4 192.168.4.51:6379
   slots:0-5460 (5461 slots) master
   1 additional replica(s)
M: c901339df646979b6deaeba512abfac5e7be6ec6 192.168.4.53:6379
   slots:10923-16383 (5461 slots) master
   1 additional replica(s)
S: 07acdc56acfe9c6c41f8390e635ebab6a87d0848 192.168.4.57:6379
   slots: (0 slots) slave
   replicates a3424220d0fbb87210a4aad2e9568ca0057788ab
S: 441b538321343a55bcc4fb60fb0f6a9fdcb98d2b 192.168.4.56:6379
   slots: (0 slots) slave
   replicates 81efbb654e1f56b313c8ed873d21c680b5a20df4
S: 279b5a30af0cb3a2277dacc8d8fd8c072a667c04 192.168.4.54:6379
   slots: (0 slots) slave
   replicates c901339df646979b6deaeba512abfac5e7be6ec6
M: a3424220d0fbb87210a4aad2e9568ca0057788ab 192.168.4.52:6379
   slots:5461-10922 (5462 slots) master
   1 additional replica(s)
[OK] All nodes agree about slots configuration.
>>> Check for open slots...
>>> Check slots coverage...
[OK] All 16384 slots covered.
[root@mgm58 ~]# 
		2.2.3  查看集群信息
[root@mgm58 ~]# redis-trib.rb  info  192.168.4.57:6379
192.168.4.53:6379 (c901339d...) -> 0 keys | 5461 slots | 1 slaves.
192.168.4.52:6379 (a3424220...) -> 0 keys | 5462 slots | 1 slaves.
192.168.4.51:6379 (81efbb65...) -> 0 keys | 5461 slots | 1 slaves.
[OK] 0 keys in 3 masters.
0.00 keys per slot on average.
[root@mgm58 ~]# 

[root@mgm58 ~]# redis-trib.rb  check  192.168.4.51:6379
>>> Performing Cluster Check (using node 192.168.4.51:6379)
M: 81efbb654e1f56b313c8ed873d21c680b5a20df4 192.168.4.51:6379
   slots:0-5460 (5461 slots) master
   1 additional replica(s)
M: c901339df646979b6deaeba512abfac5e7be6ec6 192.168.4.53:6379
   slots:10923-16383 (5461 slots) master
   1 additional replica(s)
S: 07acdc56acfe9c6c41f8390e635ebab6a87d0848 192.168.4.57:6379
   slots: (0 slots) slave
   replicates a3424220d0fbb87210a4aad2e9568ca0057788ab
S: 441b538321343a55bcc4fb60fb0f6a9fdcb98d2b 192.168.4.56:6379
   slots: (0 slots) slave
   replicates 81efbb654e1f56b313c8ed873d21c680b5a20df4
S: 279b5a30af0cb3a2277dacc8d8fd8c072a667c04 192.168.4.54:6379
   slots: (0 slots) slave
   replicates c901339df646979b6deaeba512abfac5e7be6ec6
M: a3424220d0fbb87210a4aad2e9568ca0057788ab 192.168.4.52:6379
   slots:5461-10922 (5462 slots) master
   1 additional replica(s)
[OK] All nodes agree about slots configuration.
>>> Check for open slots...
>>> Check slots coverage...
[OK] All 16384 slots covered.
[root@mgm58 ~]# 
      
	测试配置
		1  在网站服务器web33 he  web44的 命令行 连接集群存/取数据

[root@mysql52 ~]# which  redis-cli
/usr/local/bin/redis-cli
[root@mysql52 ~]# 
[root@mysql52 ~]# scp /usr/local/bin/redis-cli  root@192.168.4.33:/usr/local/bin/

[root@web33 ~]# ls /usr/local/bin/redis-cli 
/usr/local/bin/redis-cli
[root@web33 ~]# 
[root@web33 ~]# redis-cli  -c -h 192.168.4.51 -p 6379
192.168.4.51:6379> keys *
(empty list or set)
192.168.4.51:6379> set name plj
-> Redirected to slot [5798] located at 192.168.4.52:6379
OK
192.168.4.52:6379> set school tarena
OK
192.168.4.52:6379> set class 2002
OK
192.168.4.52:6379> set sex boy
-> Redirected to slot [2584] located at 192.168.4.51:6379
OK
192.168.4.51:6379> keys *
1) "sex"
192.168.4.51:6379> exit
[root@web33 ~]# 
[root@web33 ~]# 

[root@mysql52 ~]# scp /usr/local/bin/redis-cli  root@192.168.4.44:/usr/local/bin/

[root@web44 ~]# redis-cli  -c  -h 192.168.4.52 -p 6379
192.168.4.52:6379> keys *
1) "class"
2) "school"
3) "name"
192.168.4.52:6379> get sex 
-> Redirected to slot [2584] located at 192.168.4.51:6379
"boy"
192.168.4.51:6379> exit
[root@web44 ~]# 
		

		配置网站服务器可以使用PHP脚本 连接集群主机存/取数据 时间20分钟到 14:44
				1 配置网站服务器 web33 和 web44
[root@web44 ~]# which php
/usr/bin/php		
[root@web44 ~]# rpm -qf /usr/bin/php
php-cli-5.4.16-45.el7.x86_64
[root@web44 ~]# 
[root@web44 ~]# php -m
[PHP Modules]
bz2
calendar
Core
ctype
curl
date
ereg
exif
fileinfo
filter
ftp
gettext
gmp
hash
iconv
json
libxml
mhash
mysql
mysqli
openssl
pcntl
pcre
PDO
pdo_mysql
pdo_sqlite
Phar
readline
Reflection
session
shmop
SimpleXML
sockets
SPL
sqlite3
standard
tokenizer
xml
zip
zlib

[Zend Modules]

[root@web44 ~]# php -m | grep -i  redis
[root@web44 ~]# 
		
[root@web44 ~]# rpm -q php-devel gcc
package php-devel is not installed
gcc-4.8.5-28.el7.x86_64
[root@web44 ~]# 
[root@web44 ~]# yum -y install php-devel

   91  tar -zxvf /var/ftp/pub/redis-cluster-4.3.0.tgz 

   93  cd redis-4.3.0/
   95  phpize 
   97  ls /usr/bin/php-config 
   98  ./configure  --with-php-config=/usr/bin/php-config
   99  make

[root@web44 redis-4.3.0]# make install
Installing shared extensions:     /usr/lib64/php/modules/
[root@web44 redis-4.3.0]#

[root@web44 redis-4.3.0]# ls /usr/lib64/php/modules/redis.so 
/usr/lib64/php/modules/redis.so
[root@web44 redis-4.3.0]# 

[root@web44 redis-4.3.0]# vim +728 /etc/php.ini
extension_dir = "/usr/lib64/php/modules/"
extension = "redis.so"

:wq
[root@web44 redis-4.3.0]# php -m  | grep  -i redis
redis
[root@web44 redis-4.3.0]# 


在web33主机做同样的配置
   82  php -m | grep -i redis
   83  rpm -q gcc  php-devel
   84  yum -y install  php-devel
   85  tar -zxvf /var/ftp/pub/redis-cluster-4.3.0.tgz 
   86  ls
   87  cd redis-4.3.0/
   88  phpize 
   89  ./configure  --with-php-config=/usr/bin/php-config
   90  make && make install
   91  ls /usr/lib64/php/modules/
   92  vim /etc/php.ini 
   93  systemctl  restart php-fpm
   94  php -m | grep -i redis

	休息  到  15:10 

			2  编写PHP脚本连接集群存/取数据

[root@nfs30 ~]# vim /site/redis_set.php     存数据 脚本
[root@nfs30 ~]# ls /site/
a.html  b.html  c.php  d.php  redis_set.php  test.html
[root@nfs30 ~]# 
[root@nfs30 ~]# cat /site/redis_set.php 
<?php
$redis_list = ['192.168.4.51:6379','192.168.4.52:6379','192.168.4.53:6379','192.168.4.54:6379','192.168.4.56:6379','192.168.4.57:6379'];


$client = new RedisCluster(NUll,$redis_list);
$client->set("i","tarenaA ");
$client->set("j","tarenaB ");
$client->set("k","tarenaC ");
echo  "ok"; 
echo "\n" ;
?>
[root@nfs30 ~]#

[root@nfs30 ~]# vim /site/redis_get.php 取数据的脚本
[root@nfs30 ~]# 
[root@nfs30 ~]# ls /site/
a.html  b.html  c.php  d.php  redis_get.php  redis_set.php  test.html
[root@nfs30 ~]# 
[root@nfs30 ~]# cat /site/redis_get.php
<?php
$redis_list = ['192.168.4.51:6379','192.168.4.52:6379','192.168.4.53:6379','192.168.4.54:6379','192.168.4.56:6379','192.168.4.57:6379'];


$client = new RedisCluster(NUll,$redis_list);

echo $client->get("i");
echo $client->get("j");
echo $client->get("k");

echo  "\n";
?>
[root@nfs30 ~]# 



			3 客户端 访问网站的PHP脚本 

[root@mysql57 ~]# curl  http://192.168.4.33/redis_set.php
ok 
[root@mysql57 ~]# curl  http://192.168.4.44/redis_get.php
tarenaA tarenaB tarenaC 
[root@mysql57 ~]# 

		命令访问redis服务器 查看数据
[root@mysql57 ~]# redis-cli  -c -h 192.168.4.57 -p 6379
192.168.4.57:6379> keys *
1) "k"
2) "c"
3) "name"
4) "school"
5) "class"
192.168.4.57:6379> exit
[root@mysql57 ~]# redis-cli  -c -h 192.168.4.51 -p 6379
192.168.4.51:6379> keys *
1) "b"
2) "j"
3) "sex"
192.168.4.51:6379> 


三、数据迁移
	3.1 测试读写分离结构能够正常提供服务
[root@web33 ~]# mysql -h192.168.4.77 -P4006 -uadmin -p123qqq...A
mysql> select  * from db1.a;
+------+
| id   |
+------+
| 1088 |
| 1099 |
|  666 |
+------+
3 rows in set (0.00 sec)

mysql> insert into  db1.a values(8866);
Query OK, 1 row affected (0.00 sec)

mysql> insert into  db1.a values(8866);
Query OK, 1 row affected (0.00 sec)

mysql> select  * from db1.a;
+------+
| id   |
+------+
| 1088 |
| 1099 |
|  666 |
| 8866 |
| 8866 |
+------+
5 rows in set (0.00 sec)

mysql> 




	3.2 配置数据迁移
		3.2.1   把主机192.168.4.66 配置 192.168.4.11的从数据库服务器
			3.2.1.1  安装软件mysql-5.7.17.tar  并启动mysql服务  设置管理登录名123bbb...A

			
			3.2.1.2 修改配置文件 （指定server_id=66）

[root@pxcnode66 ~]# vim  /etc/my.cnf
[mysqld]
server_id=66
:wq
	
			3.2.1.3 启动mysqld服务 
	[root@pxcnode66 ~]# systemctl  restart mysqld

			 完成以上配置 10分钟 到  16:24 

			3.2.1.4确保数据一致 (使用innobackupex命令)
				1  在主服务器11 在线做数据的完全备份

[root@mysqL11 ~]# yum -y install /var/ftp/pub/libev-4.15-1.el6.rf.x86_64.rpm
[root@mysqL11 ~]# yum -y install /var/ftp/pub/percona-xtrabackup-24-2.4.7-1.el7.x86_64.rpm 
[root@mysqL11 ~]# which  innobackupex
/usr/bin/innobackupex
[root@mysqL11 ~]# 

[root@mysqL11 ~]# innobackupex  --user root --password 123qqq...A  --slave-info   /allbak --no-timestamp

[root@mysqL11 ~]# ls /allbak
backup-my.cnf   mysql                   xtrabackup_checkpoints
db1             performance_schema      xtrabackup_info
ib_buffer_pool  sys                     xtrabackup_logfile
ibdata1         xtrabackup_binlog_info
[root@mysqL11 ~]# 

[root@mysqL11 ~]# scp -r /allbak root@192.168.4.66:/opt/

				2  66主机使用11共享的备份文件恢复数据

[root@pxcnode66 ~]# yum -y install /var/ftp/pub/libev-4.15-1.el6.rf.x86_64.rpm
[root@pxcnode66 ~]# yum -y install /var/ftp/pub/percona-xtrabackup-24-2.4.7-1.el7.x86_64.rpm

[root@pxcnode66 ~]# systemctl  stop mysqld
[root@pxcnode66 ~]# rm -rf /var/lib/mysql/*
[root@pxcnode66 ~]# innobackupex --apply-log /opt/allbak/
[root@pxcnode66 ~]# innobackupex --copy-back /opt/allbak/

[root@pxcnode66 ~]# ls /var/lib/mysql
db1             ib_logfile1         sys
ib_buffer_pool  ibtmp1              xtrabackup_binlog_pos_innodb
ibdata1         mysql               xtrabackup_info
ib_logfile0     performance_schema
[root@pxcnode66 ~]# 
[root@pxcnode66 ~]# chown  -R mysql:mysql /var/lib/mysql
[root@pxcnode66 ~]# systemctl  start mysqld
[root@pxcnode66 ~]# 
[root@pxcnode66 ~]# netstat  -utnlp  | grep 3306
tcp6       0      0 :::3306                 :::*                    LISTEN      1539/mysqld         
[root@pxcnode66 ~]# 

[root@pxcnode66 ~]# mysql -uroot -p123qqq...A -e 'show databases'
mysql: [Warning] Using a password on the command line interface can be insecure.
+--------------------+
| Database           |
+--------------------+
| information_schema |
| db1                |
| mysql              |
| performance_schema |
| sys                |
+--------------------+
[root@pxcnode66 ~]#


[root@pxcnode66 ~]# grep master11 /opt/allbak/xtrabackup_info 
binlog_pos = filename 'master11.000001', position '2864'
[root@pxcnode66 ~]# 

		

			3.2.1.5指定主服务器 (change master  to)
[root@pxcnode66 ~]# mysql -uroot -p123qqq...A
mysql> change master to master_host="192.168.4.11" , master_user="repluser" , master_password="123qqq...A" , master_log_file="master11.000001" ,  master_log_pos=2864;

mysql> start slave;
mysql> exit

			3.2.1.6查看状态信息  show  slave  status\G;

[root@pxcnode66 ~]# mysql -uroot -p123qqq...A -e 'show slave status \G' | grep  -i  "master_host"
mysql: [Warning] Using a password on the command line interface can be insecure.
                  Master_Host: 192.168.4.11
[root@pxcnode66 ~]# 
[root@pxcnode66 ~]# 
[root@pxcnode66 ~]# mysql -uroot -p123qqq...A -e 'show slave status \G' | grep  -i  "yes"
mysql: [Warning] Using a password on the command line interface can be insecure.
             Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
[root@pxcnode66 ~]#
		             3.2.1.7 测试数据同步
[root@web33 ~]# mysql -h192.168.4.77 -P4006 -uadmin -p123qqq...A -e 'insert into db1.a values(7788)'

[root@web33 ~]# mysql -h192.168.4.77 -P4006 -uadmin -p123qqq...A -e 'insert into db1.a values(7788)'


[root@pxcnode66 ~]# mysql -uroot -p123qqq...A -e  'select  * from db1.a '
mysql: [Warning] Using a password on the command line interface can be insecure.
+------+
| id   |
+------+
| 1088 |
| 1099 |
|  666 |
| 8866 |
| 8866 |
| 7788 |
| 7788 |
+------+
[root@pxcnode66 ~]# 

++++++++++++++++++++++++++++++++++++++++++++++
		
		3.2.2  创建PXC集群    （ 练习+排错  1个小时  到  11:21） 
			在192.168.4.66 主机做如下配置
]# systemctl  stop mysqld

]#rpm -e --nodeps mysql-community-server mysql-community-embedded-compat mysql-community-common mysql-community-client mysql-community-devel mysql-community-test mysql-community-libs-compat mysql-community-minimal-debuginfo mysql-community-libs mysql-community-embedded mysql-community-embedded-devel
   
]# rpm -qa  | grep  mysql

]# ls /var/lib/mysql/   (不要删除数据)

]# rm -rf /etc/my.cnf

   47  rpm -ivh /var/ftp/pub/libev-4.15-1.el6.rf.x86_64.rpm 

   48  yum -y install /var/ftp/pub/pxc/percona-xtrabackup-24-2.4.13-1.el7.x86_64.rpm 

   49  tar -xvf /var/ftp/pub/pxc/Percona-XtraDB-Cluster-5.7.25-31.35-r463-el7-x86_64-bundle.tar 
   50  ls Percona-XtraDB-Cluster-*

   51  yum -y install /var/ftp/pub/pxc/qpress-1.1-14.11.x86_64.rpm 

   52  yum -y install Percona-XtraDB-Cluster-*.rpm

   53  ls /etc/percona-xtradb-cluster.conf.d/

]# vim /etc/percona-xtradb-cluster.conf.d/mysqld.cnf
[mysqld]
server_id=66
:wq

]# vim /etc/percona-xtradb-cluster.conf.d/wsrep.cnf
wsrep_cluster_address=gcomm:          //不需要写ip地址
wsrep_node_address=192.168.4.66 
wsrep_cluster_name=pxc-cluster
wsrep_node_name=pxcnode66
wsrep_sst_auth="sstuser:123qqq...A"
:wq

   59  ls /var/lib/mysql
   60  systemctl  start mysql
   61  netstat  -utnlp  | grep 3306

   63  mysql -uroot -p123qqq...A
 mysql> grant reload , lock tables , replication  client ,  process  on *.* to sstuser@"localhost" identified by "123qqq...A";

mysql>  show  status  like "%wsrep%"; 
wsrep_cluster_status           Primary 
wsrep_connected             ON  

mysql>  show  slave status  \G;
mysql>  select  * from db1.a;



reload 导入数据 , lock tables 锁表  , replication  client  查看MySQL服务状态,  process 命令停止MySQL服务

			在192.168.4.10 主机做如下配置

 
如果安装了其他版本的数据库服务软件要清除，具体操作如下：
    4  rpm -qa  | grep -i mysql
    5  systemctl  stop mysqld
    6  rm -rf /var/lib/mysql/* 
    9  rm -rf /etc/my.cnf
[root@pxcnode10 ~]# rpm -e --nodeps mysql-community-server mysql-community-embedded-compat mysql-community-common mysql-community-client mysql-community-devel mysql-community-test mysql-community-libs-compat mysql-community-minimal-debuginfo mysql-community-libs mysql-community-embedded mysql-community-embedded-devel

	安装PXC 集群软件
   14  rpm -ivh /var/ftp/pub/pxc/libev-4.15-1.el6.rf.x86_64.rpm 
   15  yum -y install /var/ftp/pub/pxc/percona-xtrabackup-24-2.4.13-1.el7.x86_64.rpm 
   16  yum -y install /var/ftp/pub/pxc/qpress-1.1-14.11.x86_64.rpm 
   17  tar -xvf /var/ftp/pub/pxc/Percona-XtraDB-Cluster-5.7.25-31.35-r463-el7-x86_64-bundle.tar 
   18  ls *.rpm
   19  yum -y install Percona-XtraDB-Cluster-*.rpm

[root@pxcnode10 ~]# ls /etc/percona-xtradb-cluster.conf.d/
mysqld.cnf  mysqld_safe.cnf  wsrep.cnf
[root@pxcnode10 ~]# 



[root@pxcnode88 ~]# ls /etc/percona-xtradb-cluster.conf.d/
mysqld.cnf  mysqld_safe.cnf  wsrep.cnf
[root@pxcnode88 ~]# 

]# vim /etc/percona-xtradb-cluster.conf.d/mysqld.cnf
[mysqld]
server-id=10
]# vim /etc/percona-xtradb-cluster.conf.d/wsrep.cnf
wsrep_cluster_address=gcomm://192.168.4.66,192.168.4.10
wsrep_node_address=192.168.4.10
wsrep_cluster_name=pxc-cluster
wsrep_node_name=pxcnode10
wsrep_sst_auth="sstuser:123qqq...A"
:wq

   59  ls /var/lib/mysql
   60  systemctl  start mysql
   61  netstat  -utnlp  | grep 3306

   63  mysql -uroot -p123qqq...A

mysql>  show  status  like "%wsrep%"; 
wsrep_cluster_status           Primary 
wsrep_connected             ON  

mysql>  select  * from db1.a;
mysql> select  user from mysql.user where user="sstuser";



   		在192.168.4.88 主机做如下配置

 
			如果安装了其他版本的数据库服务软件要清除，具体操作如下：
    4  rpm -qa  | grep -i mysql
    5  systemctl  stop mysqld
    6  rm -rf /var/lib/mysql/* 
    9  rm -rf /etc/my.cnf
[root@pxcnode10 ~]# rpm -e --nodeps mysql-community-server mysql-community-embedded-compat mysql-community-common mysql-community-client mysql-community-devel mysql-community-test mysql-community-libs-compat mysql-community-minimal-debuginfo mysql-community-libs mysql-community-embedded mysql-community-embedded-devel

			安装PXC 集群软件
   14  rpm -ivh /var/ftp/pub/pxc/libev-4.15-1.el6.rf.x86_64.rpm 
   15  yum -y install /var/ftp/pub/pxc/percona-xtrabackup-24-2.4.13-1.el7.x86_64.rpm 
   16  yum -y install /var/ftp/pub/pxc/qpress-1.1-14.11.x86_64.rpm 
   17  tar -xvf /var/ftp/pub/pxc/Percona-XtraDB-Cluster-5.7.25-31.35-r463-el7-x86_64-bundle.tar 
   18  ls *.rpm
   19  yum -y install Percona-XtraDB-Cluster-*.rpm

[root@pxcnode88 ~]# ls /etc/percona-xtradb-cluster.conf.d/
mysqld.cnf  mysqld_safe.cnf  wsrep.cnf
[root@pxcnode88 ~]# 



[root@pxcnode88 ~]# ls /etc/percona-xtradb-cluster.conf.d/
mysqld.cnf  mysqld_safe.cnf  wsrep.cnf
[root@pxcnode88 ~]# 

]# vim /etc/percona-xtradb-cluster.conf.d/mysqld.cnf
[mysqld]
server-id=88
]# vim /etc/percona-xtradb-cluster.conf.d/wsrep.cnf
wsrep_cluster_address=gcomm://192.168.4.66
wsrep_node_address=192.168.4.88
wsrep_cluster_name=pxc-cluster
wsrep_node_name=pxcnode88
wsrep_sst_auth="sstuser:123qqq...A"
:wq

   59  ls /var/lib/mysql
   60  systemctl  start mysql
   61  netstat  -utnlp  | grep 3306

   63  mysql -uroot -p123qqq...A

mysql>  show  status  like "%wsrep%"; 
wsrep_cluster_status           Primary 
wsrep_connected             ON  

mysql>  select  * from db1.a;
mysql> select  user from mysql.user where user="sstuser";
			
		在3台数据库服务器上分别修改集群成员列表的配置
]# vim /etc/percona-xtradb-cluster.conf.d/wsrep.cnf  	//pxcnode88主机
wsrep_cluster_address=gcomm://192.168.4.66,192.168.4.10,192.168.4.88

]# vim /etc/percona-xtradb-cluster.conf.d/wsrep.cnf  	//pxcnode10主机
wsrep_cluster_address=gcomm://192.168.4.66,192.168.4.88,192.168.4.10

]# vim /etc/percona-xtradb-cluster.conf.d/wsrep.cnf 	//pxcnode66主机
wsrep_cluster_address=gcomm://192.168.4.10,192.168.4.88,192.168.4.66


		测试PXC集群
[root@pxcnode66 ~]# mysql -uroot -p123qqq...A
mysql> alter table db1.a add  x int primary key auto_increment first;
mysql> exit
		在网站服务器访问PXC集群中任意一台服务器都可以存取数据并对方主机都可以看到数据
web33 ~ ]#  mysql  -h192.168.4.66  -uadmin  -p123qqq...A
web33 ~ ]#  mysql  -h192.168.4.10  -uadmin  -p123qqq...A 
web33 ~ ]#  mysql  -h192.168.4.88  -uadmin  -p123qqq...A
		

四、部署集群
	4.1  负载均衡集群    (20分钟时间到 16:10 )
		配置调度器192.168.4.99
    4  rpm -qa  | grep haproxy   ||  yum  -y install  haproxy
    5  rpm -qc haproxy
    6  cp /etc/haproxy/haproxy.cfg  /root/
    7  vim /etc/haproxy/haproxy.cfg  (只保留原文件中 global  和 default的配置 其他全删除)
手动添加如下行
 	
listen status     定义监控页面
        mode http
        bind *:80
        stats enable
        stats uri /admin
        stats auth admin:admin
	
listen mysql_3306  *:3306   定义LB集群
    mode    tcp
    option  tcpka
    balance roundrobin
    server  mysql_01 192.168.4.66:3306 check
    server  mysql_02 192.168.4.10:3306 check
    server  mysql_03 192.168.4.88:3306 check
:wq

[root@haproxy99 ~]# netstat  -utnlp  | grep 3306
tcp6       0      0 :::3306                 :::*                    LISTEN      928/mysqld          
[root@haproxy99 ~]# 
[root@haproxy99 ~]# systemctl  stop  mysqld ; systemctl  disable mysqld

[root@haproxy99 ~]# systemctl  start haproxy
[root@haproxy99 ~]# 
[root@haproxy99 ~]# netstat  -utnlp  | grep 3306
tcp        0      0 *:3306       0.0.0.0:*               LISTEN      1295/haproxy        
[root@haproxy99 ~]#

	客户端访问连接调度器 ，访问数据库
[root@web33 ~]# mysql -h192.168.4.99 -uadmin -p123qqq...A -e 'select @@hostname'
mysql: [Warning] Using a password on the command line interface can be insecure.
+------------+
| @@hostname |
+------------+
| pxcnode66  |
+------------+
[root@web33 ~]# 
[root@web33 ~]# mysql -h192.168.4.99 -uadmin -p123qqq...A -e 'select @@hostname'
mysql: [Warning] Using a password on the command line interface can be insecure.
+------------+
| @@hostname |
+------------+
| pxcnode10  |
+------------+
[root@web33 ~]# mysql -h192.168.4.99 -uadmin -p123qqq...A -e 'select @@hostname'
mysql: [Warning] Using a password on the command line interface can be insecure.
+------------+
| @@hostname |
+------------+
| pxcnode88  |
+------------+
[root@web33 ~]#	

		访问监控页面查看监控状态（打开真机的浏览器访问）
http://192.168.4.99/admin	
	用户名 admin
	密  码  admin			

	4.2  高可用集群 （配置备用调度器 192.168.4.98）
		4.2.1  配置192.168.4.98 主机可以接受访问数据库请求，并把请求可以平均分发给数据库服务器192.168.4.66   192.168.4.10  192.168.4.88


[root@haproxy98 ~]# rpm -q haproxy
haproxy-1.5.18-7.el7.x86_64
[root@haproxy98 ~]# 

[root@haproxy98 ~]# scp root@192.168.4.99:/etc/haproxy/haproxy.cfg  /etc/haproxy/

[root@haproxy98 ~]# vim /etc/haproxy/haproxy.cfg
   listen mysql_3306  8:3306   修改地址为 192.168.4.98
:wq


[root@haproxy98 ~]# netstat  -utnlp  | grep  3306
tcp6       0      0 :::3306                 :::*                    LISTEN      930/mysqld          
[root@haproxy98 ~]# systemctl  stop mysqld ; systemctl  disable mysqld
Removed symlink /etc/systemd/system/multi-user.target.wants/mysqld.service.
[root@haproxy98 ~]# 
[root@haproxy98 ~]# systemctl  start haproxy
[root@haproxy98 ~]# netstat  -utnlp  | grep  3306
tcp        0      0  8:3306       0.0.0.0:*               LISTEN      1321/haproxy        
[root@haproxy98 ~]#
			
		测试192.168.4.98 主机的调度功能
[root@web33 ~]# mysql -h192.168.4.98 -uadmin -p123qqq...A -e 'select @@hostname'
mysql: [Warning] Using a password on the command line interface can be insecure.
+------------+
| @@hostname |
+------------+
| pxcnode66  |
+------------+
[root@web33 ~]# mysql -h192.168.4.98 -uadmin -p123qqq...A -e 'select @@hostname'
mysql: [Warning] Using a password on the command line interface can be insecure.
+------------+
| @@hostname |
+------------+
| pxcnode10  |
+------------+
[root@web33 ~]# mysql -h192.168.4.98 -uadmin -p123qqq...A -e 'select @@hostname'
mysql: [Warning] Using a password on the command line interface can be insecure.
+------------+
| @@hostname |
+------------+
| pxcnode88  |
+------------+
[root@web33 ~]#

		2.2.2 配置调度器的高可用集群
			1 分别在 192.168.4.98 和 192.168.4.99 主机安装keepalived软件
[root@haproxy99 ~]# rpm -q keepalived || yum -y install keepalived
[root@haproxy98 ~]# rpm -q keepalived || yum -y install keepalived


			 2 修改192.168.4.99主机的配置文件
[root@haproxy99 ~]# cp /etc/keepalived/keepalived.conf /root/
[root@haproxy99 ~]# sed -i  '35,$d'  /etc/keepalived/keepalived.conf

[root@haproxy99 ~]# vim /etc/keepalived/keepalived.conf
global_defs {
vrrp_iptables  		//禁止iptables
}
vrrp_instance VI_1 {
    state MASTER      		//标识
    interface ens33
    priority 150   		//优先级
    virtual_ipaddress {
        192.168.4.100   	//vip地址
    }
}
:wq

[root@haproxy99 ~]# scp /etc/keepalived/keepalived.conf  root@192.168.4.98:/etc/keepalived/


[root@haproxy98 ~]# vim /etc/keepalived/keepalived.conf
vrrp_instance VI_1 {
    state BACKUP  		//标识
    interface ens33
    priority 100  		//优先级
    virtual_ipaddress {
        192.168.4.100
    }
}
:wq

			3 启动keepalived 服务
				1   启动192.168.4.99 主机的keepalived服务
[root@haproxy99 ~]# ip addr show  | grep  192.168.4.100
[root@haproxy99 ~]# 
[root@haproxy99 ~]# systemctl  start keepalived
[root@haproxy99 ~]# 
[root@haproxy99 ~]# ip addr show  | grep  192.168.4.100
    inet 192.168.4.100/32 scope global ens33
[root@haproxy99 ~]# 

				2  启动192.168.4.98 主机的keepalived服务
[root@haproxy98 ~]# systemctl  start keepalived
[root@haproxy98 ~]# 
[root@haproxy98 ~]# ip addr show  | grep  192.168.4.100
[root@haproxy98 ~]# 

		
				3  客户连接vip地址访问数据库服务

[root@web33 ~]# mysql -h192.168.4.100 -uadmin -p123qqq...A -e 'select @@hostname'
mysql: [Warning] Using a password on the command line interface can be insecure.
+------------+
| @@hostname |
+------------+
| pxcnode10  |
+------------+
[root@web33 ~]# mysql -h192.168.4.100 -uadmin -p123qqq...A -e 'select @@hostname'
mysql: [Warning] Using a password on the command line interface can be insecure.
+------------+
| @@hostname |
+------------+
| pxcnode88  |
+------------+
[root@web33 ~]# mysql -h192.168.4.100 -uadmin -p123qqq...A -e 'select @@hostname'
mysql: [Warning] Using a password on the command line interface can be insecure.
+------------+
| @@hostname |
+------------+
| pxcnode66  |
+------------+
[root@web33 ~]# 

				4 测试高可用
					把99主机关机，客户端依然可以连接vip地址192.168.4.100 访问数据库服务

[root@haproxy98 ~]# ip addr show | grep 192.168.4.100
    inet 192.168.4.100/32 scope global ens33
[root@haproxy98 ~]# 
			
[root@web33 ~]# mysql -h192.168.4.100 -uadmin -p123qqq...A -e 'select @@hostname'
mysql: [Warning] Using a password on the command line interface can be insecure.
+------------+
| @@hostname |
+------------+
| pxcnode66  |
+------------+
[root@web33 ~]# mysql -h192.168.4.100 -uadmin -p123qqq...A -e 'select @@hostname'
mysql: [Warning] Using a password on the command line interface can be insecure.
+------------+
| @@hostname |
+------------+
| pxcnode10  |
+------------+
[root@web33 ~]# mysql -h192.168.4.100 -uadmin -p123qqq...A -e 'select @@hostname'
mysql: [Warning] Using a password on the command line interface can be insecure.
+------------+
| @@hostname |
+------------+
| pxcnode88  |
+------------+
[root@web33 ~]# 
				


		网络连通性   监控路由器 交换机

		操作系统的运行情况

		服务器资源的使用率 （CPU   内存  磁盘空间）低于10G 

		服务运行状态

		配置监控报警

		可以是监控服务提供的监控模板

		可以写监控脚本

		










		